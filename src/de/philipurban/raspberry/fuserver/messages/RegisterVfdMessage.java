package de.philipurban.raspberry.fuserver.messages;

import de.philipurban.raspberry.Configuration;
import de.philipurban.raspberry.ConsoleOutput;
import de.philipurban.raspberry.fuserver.messages.response.RegisterVfdResponse;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
@FuMessageInformation(
        subUrl = "registerVFD",
        parameterType = ParameterType.POST,
        responseClass = RegisterVfdResponse.class
)
public class RegisterVfdMessage extends DefaultFuMessage {

    private String macAddress;

    @Override
    public Map<String, String> getParameterList() {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("macaddress", macAddress);
        return parameters;
    }

    @Override
    public void prepareMessage() {

        // disable this message if this frequency converter is already registered and FU-ID is saved
        if(Configuration.get().isSet(Configuration.Parameter.VFD_ID)) {
            setDisabled(true);
        }
        else {
            setDisabled(false);

            if(macAddress == null) {
                // get mac address
                try {

                    Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces();
                    while(enumNetworkInterfaces.hasMoreElements()){
                        NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
                        Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
                        while(enumInetAddress.hasMoreElements()){
                            InetAddress inetAddress = enumInetAddress.nextElement();

                            if(inetAddress.isSiteLocalAddress()){
                                NetworkInterface network = NetworkInterface.getByInetAddress(inetAddress);
                                byte[] mac = network.getHardwareAddress();
                                StringBuilder macAdr = new StringBuilder();
    //                            for (int i = 0; i < mac.length; i++) {
    //                                macAdr.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
    //                            }
                                for (int i = 0; i < mac.length; i++) {
                                    macAdr.append(String.format("%02X", mac[i]));
                                }
                                macAddress = macAdr.toString();
                                ConsoleOutput.printLine("System MAC-Address: " + macAddress, true);
                            }
                        }
                    }
                } catch (SocketException e){
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public ProcessStatus processResponse(Object obj) {

        ProcessStatus status = new ProcessStatus();

        if(!(obj instanceof RegisterVfdResponse)) {
            status.set("Wrong Repsonse Class Object", false);
            return status;
        }
        RegisterVfdResponse registerFuResponse = (RegisterVfdResponse)obj;

        Configuration.get().set(Configuration.Parameter.VFD_ID, registerFuResponse.getVfdid());
        status.set("FU Registered -> New FU-ID: " + registerFuResponse.getVfdid(), true);
        return status;
    }
}
