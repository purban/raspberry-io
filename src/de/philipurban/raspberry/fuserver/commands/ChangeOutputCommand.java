package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.Main;
import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;
import de.philipurban.raspberry.output.OutputEffect;
import de.philipurban.raspberry.output.OutputManager;
import de.philipurban.raspberry.output.OutputObject;

/**
 * @author Philip Urban
 */
@CommandInformation(
        name = "output"
)
public class ChangeOutputCommand extends SimpleCommand {

    @Override
    public ProcessStatus process(GetCommandResponse response) {

        ProcessStatus processStatus = new ProcessStatus();
        int outputValue;

        try {
            outputValue = Integer.parseInt(response.getParameter1());
        } catch (NumberFormatException e) {

            processStatus.set("Wrong interval parameter (integer expected)", false);
            return processStatus;
        }

        OutputEffect effect = OutputEffect.valueOf(response.getParameter2());
        if(effect == null) {
            processStatus.set("Unknown output effect: " + response.getParameter2(), false);
            return processStatus;
        }

        OutputManager outputManager = Main.getOutputManager();
        if(outputManager == null) {
            processStatus.set("OutputManager is null!", false);
            return processStatus;
        }

        OutputObject outputObject = outputManager.getOutputObject(effect);
        if(outputObject == null) {
            processStatus.set("Output is not registered!", false);
            return processStatus;
        }

        outputObject.setValue(outputValue);

        processStatus.set("Set output value to " + outputValue + "%", true);
        return processStatus;
    }
}
