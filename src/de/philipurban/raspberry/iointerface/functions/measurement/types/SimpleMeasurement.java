package de.philipurban.raspberry.iointerface.functions.measurement.types;

import de.philipurban.raspberry.iointerface.IOCatalog;

/**
 * @author Philip Urban
 */
public class SimpleMeasurement extends AbstractMeasurement {

    public SimpleMeasurement(IOCatalog.InterfaceInput interfaceInput) {

        super(interfaceInput.getName(), interfaceInput.getAddress());
    }

    @Override
    public void calculateValue() {

        byte[] raw = getRawValue();

        // on received byte contain one-third real byte (0-127) -> we have to build the real bytes first
        int[] real = new int[raw.length / 3];
        for(int i = 0; i < real.length; i++) {
            real[i] = raw[i*3] + raw[i*3 + 1] + raw[i*3 + 2];
        }

        int value = 0;
        for(int i = 0; i < real.length; i++) {
            value |= real[i] << i*8;
        }

        setValue(value);
    }
}
