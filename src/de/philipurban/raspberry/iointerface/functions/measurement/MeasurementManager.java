package de.philipurban.raspberry.iointerface.functions.measurement;

import de.philipurban.raspberry.iointerface.functions.measurement.types.Measurement;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Philip Urban
 */
public class MeasurementManager {

    private Set<Measurement> measurements = new HashSet<>();

    public void registerMeasurement(Measurement measurement) {

        measurements.add(measurement);
    }

    public Measurement getMeasurementByClass(Class<? extends Measurement> clazz) {

        for(Measurement measurement : measurements) {
            if(measurement.getClass().getName().equalsIgnoreCase(clazz.getName())) {
                return measurement;
            }
        }
        return null;
    }

    public Measurement getMeasurementByName(String name) {

        for(Measurement measurement : measurements) {
            if(measurement.getName().equalsIgnoreCase(name)) {
                return measurement;
            }
        }
        return null;
    }

    public Set<Measurement> getRegisteredMeasurements() {

        return measurements;
    }
}
