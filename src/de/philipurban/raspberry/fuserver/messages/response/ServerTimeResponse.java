package de.philipurban.raspberry.fuserver.messages.response;

/**
 * @author Philip Urban
 */
public class ServerTimeResponse {

    private String datetime; // YYYY-MM-DD HH:MM:SS. <- seconds in float

    /*
        Reformat datetime in linus date-time string "MMddhhmmYYYY.SS"
     */
    public String getLinuxDateTimeString() {

        String year = datetime.substring(0,4);
        String month = datetime.substring(5,7);
        String day = datetime.substring(8,10);
        String hour = datetime.substring(11,13);
        String minute = datetime.substring(14,16);

        return month + day + hour + minute + year;
    }
}
