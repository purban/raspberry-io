package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
public class CommandManager {

    private Map<String, Command> registeredCommands = new HashMap<>();

    public CommandManager() {

        registerInternal();
    }

    public void registerCommand(Class<? extends Command> clazz) {

        Command command = null;
        try {
            command = clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        registeredCommands.put(command.getName(), command);
    }

    public ProcessStatus processCommand(GetCommandResponse response) {

        Command command = registeredCommands.get(response.getCommand().toLowerCase());
        if(command == null) {
            ProcessStatus processStatus = new ProcessStatus();
            processStatus.set("Unknown Command '" + response.getCommand() + "'", false);
            return processStatus;
        }

        return command.process(response);
    }

    private void registerInternal() {

        registerCommand(MeasurementIntervalCommand.class);
        registerCommand(NothingCommand.class);
        registerCommand(WarningCommand.class);
        registerCommand(ChangeOutputCommand.class);
    }
}
