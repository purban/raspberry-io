package de.philipurban.raspberry.fuserver.messages.response;

/**
 * @author Philip Urban
 */
public class PushDataResponse {

    private int status;
    private String message;

    public Status getStatus() {

        switch(status) {
            case 0: return Status.SUCCESSFUL;
            case 1: return Status.UNKNOWN_FU_ID;
        }
        return Status.UNKNOWN_STATUS;
    }

    public String getMessage() {

        return message;
    }


    public enum Status {

        UNKNOWN_STATUS,
        SUCCESSFUL,
        UNKNOWN_FU_ID
    }
}
