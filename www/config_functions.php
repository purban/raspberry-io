<?php
function getConfig() {
    $config = array();
    $content = file_get_contents("./config/configuration.cfg");
    $lines = explode("\n", $content);
    foreach($lines as $line) {
        if(strpos($line, "=") == false) { continue; }
        $parts = explode("=", $line);
        $config[$parts[0]] = $parts[1];
    }
    return $config;
}
?>