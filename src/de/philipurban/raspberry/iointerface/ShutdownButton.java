package de.philipurban.raspberry.iointerface;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.io.IOException;

/**
 * @author Philip Urban
 */
public class ShutdownButton implements GpioPinListenerDigital {

    private GpioPinDigitalOutput statusLED;

    public ShutdownButton(GpioPinDigitalOutput statusLED) {

        this.statusLED = statusLED;
    }

    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {

        if(event.getState() == PinState.HIGH) {
            statusLED.low();
            shutdown();
        }
    }

    private void shutdown() {

        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("shutdown -h now");
        } catch (IOException e) {
        }
        System.exit(0);
    }
}
