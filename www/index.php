<?php
error_reporting(E_ALL);
session_start();
include "functions.php";
include "config_functions.php";


$startstop_info = "";
$config_info = "";
$interval_error = "";
$rpm_multiplier_error = "";
$energy_multiplier_error = "";
$url_error = "";
$update_success = "";
$update_info = "";
$update_error = "";
$output_multiplier_error = "";

if(isset($_POST["uuid"]) && $_POST["uuid"] == $_SESSION["uuid"]) {

    // dispatch action

    //--> start service
    if(isset($_POST["action_start"])) {
        $startstop_info = "Service started";
        restartService();
    }

    //--> stop service
    if(isset($_POST["action_stop"])) {
        $startstop_info = "Service stopped";
        stopService();
    }

    //--> save config
    if(isset($_POST["action_config"])) {

        $error = false;

        if(!isOnline($_POST["SERVER_URL"])) {
//            $error = true;
            $url_error = "Server unreachable!";
        }

        if(!is_numeric($_POST["MEASUREMENT_INTERVAL_MS"])) {
            $error = true;
            $interval_error = "Must be a number!";
        }

        if(!is_numeric($_POST["ENERGY_INPUT_MULTIPLIER"])) {
            $error = true;
            $energy_multiplier_error = "Must be a number!";
        }

        if(!is_numeric($_POST["RPM_INPUT_MULTIPLIER"])) {
            $error = true;
            $rpm_multiplier_error = "Must be a number!";
        }

        if(!is_numeric($_POST["RPM_OUTPUT_MULTIPLIER"])) {
            $error = true;
            $output_multiplier_error = "Must be a number!";
        }

        if(!$error) {
            $new_content = "";
            foreach($_POST as $key => $value) {
            if($key == "uuid" || $key == "action_config") { continue; }
                $new_content .= $key."=".$value."\n";
            }

            file_put_contents("./config/configuration.cfg", $new_content);

            $startstop_info = "Service restarted";
            $config_info = "Configuration saved";
            restartService();
        }
    }

    //--> firmware update
    if(isset($_POST["action_update"])) {
        $error = false;
        $uploaddir = "/var/ecobalance/";
        $uploadfile = $uploaddir . basename($_FILES['firmware_file']['name']);

         // check if no file selected
         if(empty($_FILES['firmware_file']['name'])) {
             $error = true;
             $update_error .= "&raquo; No file selected! <br />";
         }
        // check if zip file
        else if(substr($uploadfile, strlen($uploadfile) - 3) != "dat") {
            $error = true;
            $update_error .= "&raquo; Wrong filetype! <br />";
        }

        if(!$error) {
            // save file
            if (move_uploaded_file($_FILES['firmware_file']['tmp_name'], $uploadfile)) {
                $update_info .= "&raquo; Upload successful <br />";

                // unpack zip
                exec("sudo rm -r /var/ecobalance/temp/"); // remove old temp folder
                exec("sudo unzip -d /var/ecobalance/temp/ -P ecofmw14 ".$uploadfile); // unpack zip

                // check if version file exists
                if(!file_exists("/var/ecobalance/temp/raspberry-io/www/config/version.info")) {
                    $update_error .= "&raquo; No valid firmware file! <br/>";
                }
                // copy files
                else {
                    exec("sudo sudo cp -avr /var/ecobalance/temp/raspberry-io/ /var/ecobalance");
                    $update_success .= "&raquo; Updated firmware successful! <br />";

                    // set chmod
                    exec("chmod -R 755 /var/ecobalance/raspberry-io");
                }
            } else {
                $update_error .= "&raquo; Upload failed! <br />";
            }

            // cleanup
            exec("sudo rm -r /var/ecobalance/temp/"); // remove temp folder
            exec("sudo rm ".$uploadfile);
        }
    }
}

$_SESSION["uuid"] = time();
$config = getConfig();
$service_running = isRunning("GpioInterface");
$remote_online = isOnline($config["SERVER_URL"]);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ecobalance VFD Server - Web Interface</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<div id="content">
<div id="header"><a href="index.php"><h1>Ecobalance VFD Server - Web Interface</h1></a></div>
<div class="container">
	<h2>Status</h2>
    <form action="index.php" method="post">
        <input type="hidden" name="uuid" value="<?=$_SESSION["uuid"]?>" />
    	<table>
            <tr class="row-highlighted">
            	<td valign="top">Firmware Version</td>
                <td>
                	<input type="text" readonly="readonly" size="50" value="<?=getVersion()?>" />
                </td>
            </tr>
        	<tr>
            	<td valign="top">Service</td>
                <td>
                	<input class="<?=($service_running) ? "online" : "offline"?>" type="text" readonly="readonly" size="20" value="<?=($service_running) ? "Running" : "Stopped" ?>" />
                    <input name="action_start" <?=($service_running) ? "disabled" : "" ?> type="submit" value="Start" />
                    <input name="action_stop" <?=($service_running) ? "" : "disabled" ?> type="submit" value="Stop" />
                    <span class="info"><?=$startstop_info?></span>
                </td>
            </tr>
            <tr class="row-highlighted">
            	<td valign="top">Communication Server</td>
                <td>
                	<input class="<?=($remote_online) ? "online" : "offline"?>" type="text" readonly="readonly" size="20" value="<?=($remote_online) ? "Online" : "Offline" ?>" />
                </td>
            </tr>
    	</table>
    </form>    
</div>
<div class="container">
	<h2>Configuration</h2>
    
    <form action="index.php" method="post">
        <input type="hidden" name="uuid" value="<?=$_SESSION["uuid"]?>" />
    	<table>
        	<tr class="row-highlighted">
            	<td valign="top">Communication Server URL</td>
                <td>
                	<input name="SERVER_URL" type="text" size="50" value="<?=$config["SERVER_URL"]?>" />
                	<br /><span class="error"><?=$url_error?></span>
                </td>
            </tr>
            <tr>
            	<td valign="top">Measurement Intervall (in ms)</td>
                <td>
                	<input name="MEASUREMENT_INTERVAL_MS" type="text" size="10" value="<?=$config["MEASUREMENT_INTERVAL_MS"]?>" />
                	<span class="error"><?=$interval_error?></span>
                </td>
            </tr>
            <tr class="row-highlighted">
                <td valign="top">RPM Source</td>
                <td>
                	<select readonly name="RPM_INPUT_CHANNEL">
                	    <option <?=($config["RPM_INPUT_CHANNEL"] == "VOLTAGE_1") ? "selected" : "" ?> value="VOLTAGE_1">1st Voltage Input</option>
                    	<option disabled <?=($config["RPM_INPUT_CHANNEL"] == "VOLTAGE_2") ? "selected" : "" ?> value="VOLTAGE_2">2nd Voltage Input</option>
                        <option disabled <?=($config["RPM_INPUT_CHANNEL"] == "CURRENT_1") ? "selected" : "" ?> value="CURRENT_1">1st Current Input</option>
                        <option disabled <?=($config["RPM_INPUT_CHANNEL"] == "CURRENT_2") ? "selected" : "" ?> value="CURRENT_2">2nd Current Input</option>
                        <option disabled <?=($config["RPM_INPUT_CHANNEL"] == "FREQUENCY_1") ? "selected" : "" ?> value="FREQUENCY_1">Frequency Input</option>
                    </select>
                </td>
            </tr>
            <tr class="row-highlighted">
                <td valign="top">RPM Multiplier</td>
                <td>
                    <input name="RPM_INPUT_MULTIPLIER" type="text" size="10" value="<?=$config["RPM_INPUT_MULTIPLIER"]?>" />
                    <span class="error"><?=$rpm_multiplier_error?></span>
                    <p class="description">
                    &raquo; Voltage is measured in mV<br />
                    &raquo; Current is measured in mA<br />
                    &raquo; Frequency is measured in mHz<br />
                    e.g. if 10V &sim; 100%<br />
                    multiplier = 0.01 (10000mV * 0.01 = 100)
                    </p>
                </td>
            </tr>
            <tr>
            	<td valign="top">Energy Source</td>
                <td>
                	<select name="ENERGY_INPUT_CHANNEL">
                	    <option disabled <?=($config["ENERGY_INPUT_CHANNEL"] == "VOLTAGE_1") ? "selected" : "" ?> value="VOLTAGE_1">1st Voltage Input</option>
                    	<option <?=($config["ENERGY_INPUT_CHANNEL"] == "VOLTAGE_2") ? "selected" : "" ?> value="VOLTAGE_2">2nd Voltage Input</option>
                        <option <?=($config["ENERGY_INPUT_CHANNEL"] == "CURRENT_1") ? "selected" : "" ?> value="CURRENT_1">1st Current Input</option>
                        <option <?=($config["ENERGY_INPUT_CHANNEL"] == "CURRENT_2") ? "selected" : "" ?> value="CURRENT_2">2nd Current Input</option>
                        <option <?=($config["ENERGY_INPUT_CHANNEL"] == "FREQUENCY_1") ? "selected" : "" ?> value="FREQUENCY_1">Frequency Input</option>
                    </select>
                </td>
            </tr>
            <tr class="row-highlighted">
            	<td valign="top">Energy Multiplier</td>
                <td>
                	<input name="ENERGY_INPUT_MULTIPLIER" type="text" size="10" value="<?=$config["ENERGY_INPUT_MULTIPLIER"]?>" />
                	<span class="error"><?=$energy_multiplier_error?></span>
                    <p class="description">
                    &raquo; Voltage is measured in mV<br />
                    &raquo; Current is measured in mA<br />
                    &raquo; Frequency is measured in mHz<br />
                    e.g. if 10V &sim; 100%<br />
                    multiplier = 0.01 (10000mV * 0.01 = 100)
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top">Output Channel</td>
                <td>
                    <select name="RPM_OUTPUT_CHANNEL">
                        <option <?=($config["RPM_OUTPUT_CHANNEL"] == "VOLTAGE_1") ? "selected" : "" ?> value="VOLTAGE_1">1st Voltage Output</option>
                        <option <?=($config["RPM_OUTPUT_CHANNEL"] == "CURRENT_1") ? "selected" : "" ?> value="CURRENT_1">Current / 2nd Voltage Output</option>
                    </select>
                </td>
            </tr>
            <tr class="row-highlighted">
                <td valign="top">Output Multiplier</td>
                <td>
                    <input name="RPM_OUTPUT_MULTIPLIER" type="text" size="10" value="<?=$config["RPM_OUTPUT_MULTIPLIER"]?>" />
                    <span class="error"><?=$output_multiplier_error?></span>
                    <p class="description">
                    &raquo; Voltage is set in mV<br />
                    &raquo; Current is set in mA<br />
                    e.g. if 100% &sim; 10V<br />
                    multiplier = 100 (100 * 100 = 10000mV = 10V)
                    </p>
                </td>
            </tr>
            <tr class="row-highlighted">
            	<td>
            	    <input name="action_config" type="submit" value="Save &amp; Restart Service" />
            	    <span class="info"><?=$config_info?></span>
            	</td>
                <td></td>
            </tr>
    	</table>
    </form>
</div>
<div class="container">
	<h2>Firmware Update</h2>
    <p class="description">
    Make sure to select correct update file!<br />
    Device must be powered during the whole update process!<br />
    </p>
    <form enctype="multipart/form-data" action="index.php" method="post">
        <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
        <input type="hidden" name="uuid" value="<?=$_SESSION["uuid"]?>" />
    	<table>
        	<tr class="row-highlighted">
            	<td valign="top">Update File</td>
                <td>
                	<input name="firmware_file" type="file" size="50" maxlength="100000" accept="dat">
                </td>
            </tr>
            <tr>
            	<td>
            	    <span class="info"><?=$update_info?></span>
            	    <span class="error"><?=$update_error?></span>
            	    <span class="success"><?=$update_success?></span>
            	    <input name="action_update" type="submit" value="Update firmware" />
            	</td>
                <td></td>
            </tr>
    	</table>
    </form>
</div>
</div>
</body>
</html>
