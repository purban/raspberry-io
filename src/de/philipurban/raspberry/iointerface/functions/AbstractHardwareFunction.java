package de.philipurban.raspberry.iointerface.functions;

import de.philipurban.raspberry.iointerface.IOInterface;

/**
 * @author Philip Urban
 */
public abstract class AbstractHardwareFunction implements HardwareFunction {

    private String name;
    private int startAddress;

    protected AbstractHardwareFunction(String name, int startAddress) {

        this.name = name;
        this.startAddress = startAddress;
    }

    @Override
    public String getName() {

        return name;
    }

    @Override
    public int getStartAddress() {

        return startAddress;
    }

    @Override
    public int getByteSize() {

        return IOInterface.BYTE_SIZE;
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractHardwareFunction that = (AbstractHardwareFunction) o;

        if (startAddress != that.startAddress) return false;

        return true;
    }

    @Override
    public int hashCode() {

        return startAddress;
    }
}
