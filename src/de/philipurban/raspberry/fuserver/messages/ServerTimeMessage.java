package de.philipurban.raspberry.fuserver.messages;

import de.philipurban.raspberry.fuserver.messages.response.ServerTimeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
@FuMessageInformation(
        subUrl = "servertime",
        parameterType = ParameterType.GET,
        responseClass = ServerTimeResponse.class
)
public class ServerTimeMessage extends DefaultFuMessage {

    @Override
    public Map<String, String> getParameterList() {

        Map<String, String> parameters = new HashMap<>();
        // no parameter needed
        return parameters;
    }

    @Override
    public void prepareMessage() {

        // nothing to do
    }

    @Override
    public ProcessStatus processResponse(Object obj) {

        ProcessStatus status = new ProcessStatus();

        if(!(obj instanceof ServerTimeResponse)) {
            status.set("Wrong Repsonse Class Object", false);
            return status;
        }
        ServerTimeResponse serverTimeResponse = (ServerTimeResponse)obj;

        String dateString = serverTimeResponse.getLinuxDateTimeString();

        if(dateString != null) {
            try {
               Runtime.getRuntime().exec("date " + dateString); // MMddhhmmYYYY.SS
                status.set("Update System Time: " +
                        dateString.substring(2,4) + "." + dateString.substring(0,2) + "." + dateString.substring(8) + " - " + dateString.substring(4,6) + ":" + dateString.substring(6,8), true);
            } catch (IOException e) {
                status.set(e.getMessage(), false);
            }
        } else {
            status.set("Empty Date Time String", false);
        }
        return status;
    }
}
