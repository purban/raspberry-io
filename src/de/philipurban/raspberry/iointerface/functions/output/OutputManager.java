package de.philipurban.raspberry.iointerface.functions.output;

import de.philipurban.raspberry.iointerface.functions.output.types.Output;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Philip Urban
 */
public class OutputManager {

    private Set<Output> outputs = new HashSet<>();

    public void registerOutput(Output output) {

        outputs.add(output);
    }

    public Output getOutputByClass(Class<? extends Output> clazz) {

        for(Output output : outputs) {
            if(output.getClass().getName().equalsIgnoreCase(clazz.getName())) {
                return output;
            }
        }
        return null;
    }

    public Output getOutputByName(String name) {

        for(Output output : outputs) {
            if(output.getName().equalsIgnoreCase(name)) {
                return output;
            }
        }
        return null;
    }

    public Set<Output> getRegisteredOutputs() {

        return outputs;
    }
}
