package de.philipurban.raspberry.iointerface.functions.output.types;

import de.philipurban.raspberry.iointerface.IOCatalog;

/**
 * @author Philip Urban
 */
public class SimpleOutput extends AbstractOutput {

    public SimpleOutput(IOCatalog.InterfaceOutput output) {

        super(output.getName(), output.getAddress());
    }

    @Override
    public void calculateRawValue() {

        byte[] raw = getRawValue();
        int temp;
        for(int i = 0; i < raw.length / 3; i++) {

            temp = 0xFF & (getValue() >> (i*8));
            if(temp < 128) {
                raw[i*3] = (byte)temp;
                raw[i*3 + 1] = 0;
                raw[i*3 + 2] = 0;
            }
            else if(temp < 255) {
                raw[i*3] = (byte)(temp - 127);
                raw[i*3 + 1] = 127;
                raw[i*3 + 2] = 0;
            }
            else if(temp == 255) {
                raw[i*3] = 127;
                raw[i*3 + 1] = 127;
                raw[i*3 + 2] = 1;
            }
        }
        setRawValue(raw);
    }
}
