package de.philipurban.raspberry.fuserver;

import de.philipurban.raspberry.fuserver.messages.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Philip Urban
 */
public class CommunicationManager {

    private List<QueuedMessage> queuedMessages = new ArrayList<>();

    public CommunicationManager() {

        // register all messages
        queuedMessages.add(new QueuedMessage(new ServerTimeMessage(), 10 * 60)); // update server time each 10 minutes
        queuedMessages.add(new QueuedMessage(new RegisterVfdMessage(), 1 * 30)); // try register this frequency converter and receive FU-ID
        queuedMessages.add(new QueuedMessage(new GetCommandMessage(), 5));     // check every 30 seconds for new commands
        queuedMessages.add(new QueuedMessage(new PushDataMessage(), 1));       // send permanent data if available
    }

    public Message getMessage(Class clazz) {

        for(QueuedMessage queuedMessage : queuedMessages) {
            if(queuedMessage.getMessage().getClass().equals(clazz)) {
                return queuedMessage.getMessage();
            }
        }
        return null;
    }

    public List<QueuedMessage> getQueuedMessages() {

        return queuedMessages;
    }

    public void startCommunication() {

        Thread thread = new Thread(new HttpRequestTask(this));
        thread.start();
    }
}
