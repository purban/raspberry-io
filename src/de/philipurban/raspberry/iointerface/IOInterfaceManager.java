package de.philipurban.raspberry.iointerface;

import com.pi4j.io.i2c.I2CBus;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
public class IOInterfaceManager {

    private I2CBus bus;
    private InterfaceTask interfaceTask;
    private Map<Integer, IOInterface> interfaceList = new HashMap<>();

    public IOInterfaceManager(I2CBus bus) {

        this.bus = bus;

        interfaceTask = new InterfaceTask(this);
        Thread thread = new Thread(interfaceTask);
        thread.start();
    }

    public void registerIOInterface(int address) {

        interfaceList.put(address, new IOInterface(bus, address));
    }

    public IOInterface getIOInterface(int address) {

        return interfaceList.get(address);
    }

    public void startInterfaceCommunication() {

        interfaceTask.setRunning(true);
    }

    public void stopInterfaceCommunication() {

        interfaceTask.setRunning(false);
    }

    public boolean getInterfaceCommunicationState() {

        return interfaceTask.isRunning();
    }

    public Collection<IOInterface> getInterfaceList() {

        return interfaceList.values();
    }
}
