package de.philipurban.raspberry.fuserver.messages.response;

/**
 * @author Philip Urban
 */
public class GetCommandResponse {

    private String command;
    private int commandId;

    private String parameter1;
    private String parameter2;
    private String parameter3;
    private String parameter4;
    private String parameter5;
    private String parameter6;
    private String parameter7;
    private String parameter8;
    private String parameter9;
    private String parameter10;
    private String parameter11;
    private String parameter12;
    private String parameter13;
    private String parameter14;
    private String parameter15;
    private String parameter16;
    private String parameter17;
    private String parameter18;
    private String parameter19;
    private String parameter20;
    private String parameter21;
    private String parameter22;
    private String parameter23;
    private String parameter24;
    private String parameter25;
    private String parameter26;
    private String parameter27;
    private String parameter28;
    private String parameter29;
    private String parameter30;
    private String parameter31;
    private String parameter32;
    private String parameter33;
    private String parameter34;
    private String parameter35;
    private String parameter36;
    private String parameter37;
    private String parameter38;
    private String parameter39;
    private String parameter40;
    private String parameter41;
    private String parameter42;
    private String parameter43;
    private String parameter44;
    private String parameter45;
    private String parameter46;
    private String parameter47;
    private String parameter48;
    private String parameter49;
    private String parameter50;
    private String parameter51;
    private String parameter52;
    private String parameter53;
    private String parameter54;
    private String parameter55;
    private String parameter56;
    private String parameter57;
    private String parameter58;
    private String parameter59;
    private String parameter60;
    private String parameter61;
    private String parameter62;
    private String parameter63;
    private String parameter64;
    private String parameter65;
    private String parameter66;
    private String parameter67;
    private String parameter68;
    private String parameter69;
    private String parameter70;
    private String parameter71;
    private String parameter72;
    private String parameter73;
    private String parameter74;
    private String parameter75;
    private String parameter76;
    private String parameter77;
    private String parameter78;
    private String parameter79;
    private String parameter80;
    private String parameter81;
    private String parameter82;
    private String parameter83;
    private String parameter84;
    private String parameter85;
    private String parameter86;
    private String parameter87;
    private String parameter88;
    private String parameter89;
    private String parameter90;
    private String parameter91;
    private String parameter92;
    private String parameter93;
    private String parameter94;
    private String parameter95;
    private String parameter96;
    private String parameter97;
    private String parameter98;
    private String parameter99;

    public int getCommandNumber() {

        return commandId;
    }

    public String getParameter1() {

        return parameter1;
    }

    public String getParameter2() {

        return parameter2;
    }

    public String getParameter3() {

        return parameter3;
    }

    public String getParameter4() {

        return parameter4;
    }

    public String getParameter5() {

        return parameter5;
    }

    public String getParameter6() {

        return parameter6;
    }

    public String getParameter7() {

        return parameter7;
    }

    public String getParameter8() {

        return parameter8;
    }

    public String getParameter9() {

        return parameter9;
    }

    public String getParameter10() {

        return parameter10;
    }

    public String getParameter11() {

        return parameter11;
    }

    public String getParameter12() {

        return parameter12;
    }

    public String getParameter13() {

        return parameter13;
    }

    public String getParameter14() {

        return parameter14;
    }

    public String getParameter15() {

        return parameter15;
    }

    public String getParameter16() {

        return parameter16;
    }

    public String getParameter17() {

        return parameter17;
    }

    public String getParameter18() {

        return parameter18;
    }

    public String getParameter19() {

        return parameter19;
    }

    public String getParameter20() {

        return parameter20;
    }

    public String getParameter21() {

        return parameter21;
    }

    public String getParameter22() {

        return parameter22;
    }

    public String getParameter23() {

        return parameter23;
    }

    public String getParameter24() {

        return parameter24;
    }

    public String getParameter25() {

        return parameter25;
    }

    public String getParameter26() {

        return parameter26;
    }

    public String getParameter27() {

        return parameter27;
    }

    public String getParameter28() {

        return parameter28;
    }

    public String getParameter29() {

        return parameter29;
    }

    public String getParameter30() {

        return parameter30;
    }

    public String getParameter31() {

        return parameter31;
    }

    public String getParameter32() {

        return parameter32;
    }

    public String getParameter33() {

        return parameter33;
    }

    public String getParameter34() {

        return parameter34;
    }

    public String getParameter35() {

        return parameter35;
    }

    public String getParameter36() {

        return parameter36;
    }

    public String getParameter37() {

        return parameter37;
    }

    public String getParameter38() {

        return parameter38;
    }

    public String getParameter39() {

        return parameter39;
    }

    public String getParameter40() {

        return parameter40;
    }

    public String getParameter41() {

        return parameter41;
    }

    public String getParameter42() {

        return parameter42;
    }

    public String getParameter43() {

        return parameter43;
    }

    public String getParameter44() {

        return parameter44;
    }

    public String getParameter45() {

        return parameter45;
    }

    public String getParameter46() {

        return parameter46;
    }

    public String getParameter47() {

        return parameter47;
    }

    public String getParameter48() {

        return parameter48;
    }

    public String getParameter49() {

        return parameter49;
    }

    public String getParameter50() {

        return parameter50;
    }

    public String getParameter51() {

        return parameter51;
    }

    public String getParameter52() {

        return parameter52;
    }

    public String getParameter53() {

        return parameter53;
    }

    public String getParameter54() {

        return parameter54;
    }

    public String getParameter55() {

        return parameter55;
    }

    public String getParameter56() {

        return parameter56;
    }

    public String getParameter57() {

        return parameter57;
    }

    public String getParameter58() {

        return parameter58;
    }

    public String getParameter59() {

        return parameter59;
    }

    public String getParameter60() {

        return parameter60;
    }

    public String getParameter61() {

        return parameter61;
    }

    public String getParameter62() {

        return parameter62;
    }

    public String getParameter63() {

        return parameter63;
    }

    public String getParameter64() {

        return parameter64;
    }

    public String getParameter65() {

        return parameter65;
    }

    public String getParameter66() {

        return parameter66;
    }

    public String getParameter67() {

        return parameter67;
    }

    public String getParameter68() {

        return parameter68;
    }

    public String getParameter69() {

        return parameter69;
    }

    public String getParameter70() {

        return parameter70;
    }

    public String getParameter71() {

        return parameter71;
    }

    public String getParameter72() {

        return parameter72;
    }

    public String getParameter73() {

        return parameter73;
    }

    public String getParameter74() {

        return parameter74;
    }

    public String getParameter75() {

        return parameter75;
    }

    public String getParameter76() {

        return parameter76;
    }

    public String getParameter77() {

        return parameter77;
    }

    public String getParameter78() {

        return parameter78;
    }

    public String getParameter79() {

        return parameter79;
    }

    public String getParameter80() {

        return parameter80;
    }

    public String getParameter81() {

        return parameter81;
    }

    public String getParameter82() {

        return parameter82;
    }

    public String getParameter83() {

        return parameter83;
    }

    public String getParameter84() {

        return parameter84;
    }

    public String getParameter85() {

        return parameter85;
    }

    public String getParameter86() {

        return parameter86;
    }

    public String getParameter87() {

        return parameter87;
    }

    public String getParameter88() {

        return parameter88;
    }

    public String getParameter89() {

        return parameter89;
    }

    public String getParameter90() {

        return parameter90;
    }

    public String getParameter91() {

        return parameter91;
    }

    public String getParameter92() {

        return parameter92;
    }

    public String getParameter93() {

        return parameter93;
    }

    public String getParameter94() {

        return parameter94;
    }

    public String getParameter95() {

        return parameter95;
    }

    public String getParameter96() {

        return parameter96;
    }

    public String getParameter97() {

        return parameter97;
    }

    public String getParameter98() {

        return parameter98;
    }

    public String getParameter99() {

        return parameter99;
    }

    public String getCommand() {

        return command;
    }
}
