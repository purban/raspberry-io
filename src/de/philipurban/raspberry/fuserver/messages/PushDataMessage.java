package de.philipurban.raspberry.fuserver.messages;

import de.philipurban.raspberry.Configuration;
import de.philipurban.raspberry.fuserver.messages.response.PushDataResponse;
import de.philipurban.raspberry.measurements.AverageMeasurementData;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
@FuMessageInformation(
        subUrl = "pushdata",
        parameterType = ParameterType.POST,
        responseClass = PushDataResponse.class
)
public class PushDataMessage extends DefaultFuMessage {

    private AverageMeasurementData measurementData;
    private int retryCount;

    public void setMeasurementData(AverageMeasurementData measurementData) {

        this.measurementData = measurementData;
    }

    public boolean hasData() {

        if(measurementData == null) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public Map<String, String> getParameterList() {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("vfdid", Configuration.get().getString(Configuration.Parameter.VFD_ID));
        parameters.put("starttime", measurementData.getFormattedStartTime());
        parameters.put("endtime", measurementData.getFormattedEndTime());
        parameters.put("energy", String.valueOf(measurementData.getEnergy()));
        parameters.put("rpm", String.valueOf(measurementData.getRpm()));
        return parameters;
    }

    @Override
    public void prepareMessage() {

        // disable message if no measurement data is available
        if(!hasData() || !Configuration.get().isSet(Configuration.Parameter.VFD_ID)) {
            setDisabled(true);
        }
        else {
            setDisabled(false);
        }
    }

    @Override
    public ProcessStatus processResponse(Object obj) {

        ProcessStatus status = new ProcessStatus();

        if(!(obj instanceof PushDataResponse)) {
            status.set("Wrong Repsonse Class Object", false);
            return status;
        }
        PushDataResponse pushDataResponse = (PushDataResponse)obj;

        if(pushDataResponse.getStatus() == PushDataResponse.Status.SUCCESSFUL) {

            measurementData = null;
            retryCount = 0;
            status.set("Transmitted data --> Response: " + pushDataResponse.getMessage(), true);
        }
        else {

            status.set("Transmitted data (Retry #" + retryCount + ") --> Response: " + pushDataResponse.getMessage(), false);
            retryCount++;
            if(retryCount > 3) {
                measurementData = null;
                retryCount = 0;
            }
        }

        return status;
    }
}
