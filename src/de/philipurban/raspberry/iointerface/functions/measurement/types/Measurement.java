package de.philipurban.raspberry.iointerface.functions.measurement.types;

import de.philipurban.raspberry.iointerface.functions.Accuracy;
import de.philipurban.raspberry.iointerface.functions.HardwareFunction;

/**
 * @author Philip Urban
 */
public interface Measurement extends HardwareFunction {

    public byte[] getRawValue();

    public void setRawValue(byte[] value);

    public void calculateValue();

    public int getValue();

    public double getValue(Accuracy accuracy);

    public boolean equals(Object o);

    public int hashCode();
}
