package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

/**
 * @author Philip Urban
 */
@CommandInformation(
        name = "nothing"
)
public class NothingCommand extends SimpleCommand {

    @Override
    public ProcessStatus process(GetCommandResponse response) {
        // DO NOTHING
        return new ProcessStatus(true, "Nothing to do");
    }
}
