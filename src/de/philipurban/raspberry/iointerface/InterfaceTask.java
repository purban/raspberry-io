package de.philipurban.raspberry.iointerface;

import de.philipurban.raspberry.ConsoleOutput;
import de.philipurban.raspberry.iointerface.functions.measurement.types.Measurement;
import de.philipurban.raspberry.iointerface.functions.output.types.Output;

import java.io.IOException;

/**
 * @author Philip Urban
 */
public class InterfaceTask implements Runnable {

    public final static long DEVICE_UPDATE_INTERVAL = 50;

    private IOInterfaceManager ioInterfaceManager;
    private boolean running;

    public InterfaceTask(IOInterfaceManager ioInterfaceManager) {

        this.ioInterfaceManager = ioInterfaceManager;
        this.running = false;
    }

    @Override
    public void run() {

        while(true) {
            try {
                if(!running) {
                    continue;
                }

                for(IOInterface ioInterface : ioInterfaceManager.getInterfaceList()) {
                    // measurement
                    for(Measurement m : ioInterface.getMeasurementManager().getRegisteredMeasurements()) {

                        byte[] buffer = m.getRawValue();
                        try {
                            for(int i = 0; i < m.getByteSize(); i++) {
                                int read = ioInterface.getDevice().read(m.getStartAddress() + i);
                                buffer[i] = (byte)read;
                            }
                            m.setRawValue(buffer);
                        } catch (IOException e) {
                            ConsoleOutput.printError("Error while reading device data from measurement '" + m.getName() + "'", "reading");
                        }
                    }

                    // output
                    for(Output o : ioInterface.getOutputManager().getRegisteredOutputs()) {

                        try {
                            for(int i = 0; i < o.getByteSize(); i++) {
                                ioInterface.getDevice().write(o.getStartAddress() + i, o.getRawValue()[i]);
                            }
                        } catch (IOException e) {
                            ConsoleOutput.printError("Error while writing device data for output '" + o.getName() + "': ", "writing");
                        }
                    }
                }


                Thread.currentThread();
                Thread.sleep(DEVICE_UPDATE_INTERVAL);
            } catch (InterruptedException e) {}
        }
    }

    public void setRunning(boolean running) {

        this.running = running;
    }

    public boolean isRunning() {

        return running;
    }
}
