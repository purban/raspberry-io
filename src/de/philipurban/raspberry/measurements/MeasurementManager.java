package de.philipurban.raspberry.measurements;

import de.philipurban.raspberry.Configuration;
import de.philipurban.raspberry.ConsoleOutput;
import de.philipurban.raspberry.fuserver.CommunicationManager;
import de.philipurban.raspberry.fuserver.messages.PushDataMessage;
import de.philipurban.raspberry.iointerface.functions.measurement.types.Measurement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Philip Urban
 */
public class MeasurementManager {

    private CommunicationManager communicationManager;
    private List<MeasurementObject> measurements = new ArrayList<>();
    private AverageMeasurementData averageMeasurementData;
    private long lastAverageMeasurement = System.currentTimeMillis();
    private long lastInternalMeasurement = 0;

    public MeasurementManager(CommunicationManager communicationManager) {

        this.communicationManager = communicationManager;
    }

    public void addMeasurement(Measurement measurement, MeasurementEffect effect) {

        measurements.add(new MeasurementObject(measurement, effect));
    }

    private void takeMeasurement() {

        for(MeasurementObject measurementObject : measurements) {

            measurementObject.takeMeasurement();
        }
    }

    private void calculateAverage() {

        AverageMeasurementData averageData = new AverageMeasurementData();

        for(MeasurementObject measurementObject : measurements) {

            ValueSet valueSet = measurementObject.getAverage();

            if(measurementObject.getEffect() == MeasurementEffect.ENERGY) {

                averageData.setStartTime(valueSet.getStartTime());
                averageData.setEndTime(valueSet.getEndTime());
                averageData.setEnergy(valueSet.getValue());
            }

            if(measurementObject.getEffect() == MeasurementEffect.RPM) {

                averageData.setRpm(valueSet.getValue());
            }
        }
        averageMeasurementData = averageData;
    }

    public void triggerMeasurement() {

        // take new internal measurement if interval is reached
        if((System.currentTimeMillis() - lastInternalMeasurement) > Configuration.get().getInteger(Configuration.Parameter.INTERNAL_MEASUREMENT_INTERVAL_MS)) {

            takeMeasurement();
            lastInternalMeasurement = System.currentTimeMillis();
        }

        // take new average measurement if interval is reached
        if((System.currentTimeMillis() - lastAverageMeasurement) > Configuration.get().getInteger(Configuration.Parameter.MEASUREMENT_INTERVAL_MS)) {

            calculateAverage();
            // recalculate average data with config settings
            if(Configuration.get().isSet(Configuration.Parameter.ENERGY_INPUT_MULTIPLIER)) {
                double multiplier = Configuration.get().getDouble(Configuration.Parameter.ENERGY_INPUT_MULTIPLIER);
                averageMeasurementData.setEnergy(averageMeasurementData.getEnergy() * multiplier);
            }
            if(Configuration.get().isSet(Configuration.Parameter.RPM_INPUT_MULTIPLIER)) {
                double multiplier = Configuration.get().getDouble(Configuration.Parameter.RPM_INPUT_MULTIPLIER);
                averageMeasurementData.setRpm(averageMeasurementData.getRpm() * multiplier);
            }

            ConsoleOutput.printLine("Calculated average --> "
                    + "Start: " + averageMeasurementData.getFormattedStartTime() + " | "
                    + "End: " + averageMeasurementData.getFormattedEndTime() + " | "
                    + "Energy: " + averageMeasurementData.getEnergy() + " | "
                    + "RPM: " + averageMeasurementData.getRpm()
                    , true);
            lastAverageMeasurement = System.currentTimeMillis();
        }

        /*
         * check if new data can be queued
         */

        // get push message
        PushDataMessage pushDataMessage = (PushDataMessage)communicationManager.getMessage(PushDataMessage.class);
        // skip if data is already queued
        if(pushDataMessage.hasData()) {
            return;
        }

        if(averageMeasurementData != null) {
            pushDataMessage.setMeasurementData(averageMeasurementData); // set new data to transmit
            averageMeasurementData = null;
        }
    }
}
