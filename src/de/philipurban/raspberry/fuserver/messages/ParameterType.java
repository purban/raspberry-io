package de.philipurban.raspberry.fuserver.messages;

/**
 * @author Philip Urban
 */
public enum ParameterType {

    GET,
    POST
}
