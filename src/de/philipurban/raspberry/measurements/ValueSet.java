package de.philipurban.raspberry.measurements;

/**
 * @author Philip Urban
 */
public class ValueSet {

    private int value;
    private long startTime;
    private long endTime;

    public ValueSet(int value, long startTime, long endTime) {

        this.value = value;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getValue() {

        return value;
    }

    public long getStartTime() {

        return startTime;
    }

    public long getEndTime() {

        return endTime;
    }
}
