package de.philipurban.raspberry.fuserver.messages;

import de.philipurban.raspberry.Configuration;
import de.philipurban.raspberry.fuserver.commands.CommandManager;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
@FuMessageInformation(
        subUrl = "getcommand",
        parameterType = ParameterType.GET,
        responseClass = GetCommandResponse.class
)
public class GetCommandMessage extends DefaultFuMessage {

    CommandManager commandManager;

    public GetCommandMessage() {

        commandManager = new CommandManager();
    }

    @Override
    public Map<String, String> getParameterList() {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("vfdid", Configuration.get().getString(Configuration.Parameter.VFD_ID));
        parameters.put("lastcommandnumber", String.valueOf(Configuration.get().getInteger(Configuration.Parameter.LAST_COMMAND_NUMBER)));
        return parameters;
    }

    @Override
    public void prepareMessage() {

        // disable message if FU-ID is not set
        if(Configuration.get().isSet(Configuration.Parameter.VFD_ID)) {
            setDisabled(false);
        }
        else {
            setDisabled(true);
        }
    }

    @Override
    public ProcessStatus processResponse(Object obj) {

        ProcessStatus status = new ProcessStatus();

        if(!(obj instanceof GetCommandResponse)) {
            status.set("Wrong Repsonse Class Object", false);
            return status;
        }
        GetCommandResponse response = (GetCommandResponse)obj;

        status = commandManager.processCommand(response);
        status.setMessage("Received command: '" + response.getCommand() + "' -> " + status.getMessage());

        return status;
    }
}
