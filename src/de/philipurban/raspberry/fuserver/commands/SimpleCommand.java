package de.philipurban.raspberry.fuserver.commands;

/**
 * @author Philip Urban
 */
public abstract class SimpleCommand implements Command {

    String name;

    protected SimpleCommand() {

        CommandInformation annotation = getClass().getAnnotation(CommandInformation.class);
        name = annotation.name().toLowerCase();
    }

    @Override
    public String getName() {

        return name;
    }
}
