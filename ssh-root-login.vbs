﻿#$language = "VBScript"
#$interface = "1.0"

crt.Screen.Synchronous = True

' This automatically generated script may need to be
' edited in order to work correctly.

Sub Main
	crt.Screen.Send "su root" & chr(13)
	crt.Screen.WaitForString "Password: "
	crt.Screen.Send "admin" & chr(13)
	crt.Screen.WaitForString "[12:29:12] " & chr(27) & "[01;31mroot" & chr(27) & "[00m " & chr(27) & "[01;32m/home/pi" & chr(27) & "[00m: "
	crt.Screen.Send "cd /root/raspberry-io" & chr(13)
End Sub
