package de.philipurban.raspberry.fuserver.messages;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Philip Urban
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface FuMessageInformation {

    public String subUrl();

    public ParameterType parameterType();

    public Class responseClass();
}
