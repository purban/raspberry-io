package de.philipurban.raspberry;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Philip Urban
 */
public class Configuration {

    /* Singleton stuff */

    private static Configuration instance;
    private static Properties configFile = new Properties();

    public static void load(String configPath) throws IOException {

        Configuration config = get();
        InputStream inputStream = new FileInputStream(configPath);
        configFile.load(inputStream);

        for(Parameter parameter : Parameter.values()) {
            String value = configFile.getProperty(parameter.name());
            if(value == null) { continue; }

            if(parameter.getType() == Integer.class) {

                try {
                    int intValue = Integer.parseInt(value);
                    config.set(parameter, intValue);
                } catch (NumberFormatException e) {
                    continue;
                }
            }
            else if(parameter.getType() == Double.class) {

                try {
                    double doubleValue = Double.parseDouble(value);
                    config.set(parameter, doubleValue);
                } catch (NumberFormatException e) {
                    continue;
                }
            }
            else if(parameter.getType() == String.class) {
                config.set(parameter, value);
            }
        }
    }

    public static Configuration get() {

        if(instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    /* Configuration */

    private Map<String, Object> parameters = new HashMap<>();

    public void set(Parameter parameter, Object value) {

        parameters.put(parameter.name(), value);
    }

    public void unset(Parameter parameter) {

        parameters.remove(parameter.name());
    }

    public boolean isSet(Parameter parameter) {

        if(parameters.containsKey(parameter.name())) {
            return true;
        }
        else {
            return false;
        }
    }

    public int getInteger(Parameter parameter) {

        if(!parameters.containsKey(parameter.name())) {
            return 0;
        }
        return (int)parameters.get(parameter.name());
    }

    public double getDouble(Parameter parameter) {

        if(!parameters.containsKey(parameter.name())) {
            return 0;
        }
        return (double)parameters.get(parameter.name());
    }

    public boolean getBoolean(Parameter parameter) {

        if(!parameters.containsKey(parameter.name())) {
            return false;
        }
        return (boolean)parameters.get(parameter.name());
    }

    public String getString(Parameter parameter) {

        if(!parameters.containsKey(parameter.name())) {
            return "";
        }
        return (String)parameters.get(parameter.name());
    }

    /* Available config parameters */

    public enum Parameter {

        VFD_ID(Integer.class),
        SERVER_URL(String.class),
        LAST_COMMAND_NUMBER(Integer.class),
        INTERNAL_MEASUREMENT_INTERVAL_MS(Integer.class),
        MEASUREMENT_INTERVAL_MS(Integer.class),
        MEASUREMENT_DURABILITY(Integer.class),
        RPM_INPUT_CHANNEL(String.class),
        RPM_INPUT_MULTIPLIER(Double.class),
        ENERGY_INPUT_CHANNEL(String.class),
        ENERGY_INPUT_MULTIPLIER(Double.class),
        RPM_OUTPUT_CHANNEL(String.class),
        RPM_OUTPUT_MULTIPLIER(Double.class);

        private Class type;

        private Parameter(Class clazz) {
            this.type = clazz;
        }

        public Class getType() {

            return type;
        }
    }
}
