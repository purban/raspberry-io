package de.philipurban.raspberry.fuserver.messages;

import java.util.Map;

/**
 * @author Philip Urban
 */
public interface Message {

    public String getUrl();

    public Map<String, String> getParameterList();

    public Class getResponseClass();

    public void prepareMessage();

    public ProcessStatus processResponse(Object obj);

    public ParameterType getParameterType();

    public boolean isDisabled();
}
