package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.Configuration;
import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

/**
 * @author Philip Urban
 */
@CommandInformation(
        name = "interval"
)
public class MeasurementIntervalCommand extends SimpleCommand {

    @Override
    public ProcessStatus process(GetCommandResponse response) {

        ProcessStatus processStatus = new ProcessStatus();
        int newInterval;

        try {
            newInterval = Integer.parseInt(response.getParameter1());
        } catch (NumberFormatException e) {

            processStatus.set("Wrong interval parameter (integer expected)", false);
            return processStatus;
        }

        Configuration.get().set(Configuration.Parameter.MEASUREMENT_INTERVAL_MS, newInterval);

        processStatus.set("Set measurement interval to " + newInterval + "ms", true);
        return processStatus;
    }
}
