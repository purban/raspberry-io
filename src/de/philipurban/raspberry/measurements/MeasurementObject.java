package de.philipurban.raspberry.measurements;

import de.philipurban.raspberry.iointerface.functions.measurement.types.Measurement;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Philip Urban
 */
public class MeasurementObject {

    private Measurement measurement;
    private List<ValueSet> measurementResults = new LinkedList<>();
    MeasurementEffect effect;

    public MeasurementObject(Measurement measurement, MeasurementEffect effect) {

        this.measurement = measurement;
        this.effect = effect;
    }

    public Measurement getMeasurement() {

        return measurement;
    }

    public MeasurementEffect getEffect() {

        return effect;
    }

    public void takeMeasurement() {

        measurementResults.add(new ValueSet(measurement.getValue(), System.currentTimeMillis(), 0));
    }

    public ValueSet getAverage() {

        long start = 0;
        long end = 0;
        int average = 0;

        for(ValueSet valueSet : measurementResults) {

            if(valueSet.getStartTime() < start || start == 0) {
                start = valueSet.getStartTime();
            }
            if(valueSet.getStartTime() > end) {
                end = valueSet.getStartTime();
            }
            average += valueSet.getValue();
        }

        average /= measurementResults.size();
        measurementResults.clear();

        return new ValueSet(average, start, end);
    }
}
