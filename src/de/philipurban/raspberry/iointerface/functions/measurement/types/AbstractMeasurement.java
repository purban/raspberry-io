package de.philipurban.raspberry.iointerface.functions.measurement.types;

import de.philipurban.raspberry.iointerface.IOInterface;
import de.philipurban.raspberry.iointerface.functions.AbstractHardwareFunction;
import de.philipurban.raspberry.iointerface.functions.Accuracy;

/**
 * @author Philip Urban
 */
public abstract class AbstractMeasurement extends AbstractHardwareFunction implements Measurement {

    private int value;
    private byte[] rawValue = new byte[IOInterface.BYTE_SIZE];

    protected AbstractMeasurement(String name, int startAddress) {

        super(name, startAddress);
    }

    @Override
    public void setRawValue(byte[] rawValue) {

        this.rawValue = rawValue;
        calculateValue();
    }

    @Override
    public byte[] getRawValue() {

        return rawValue;
    }

    @Override
    public int getValue() {

        return value;
    }

    @Override
    public double getValue(Accuracy accuracy) {

        return Math.round(getValue() * (double)accuracy.getAccuracy() / (double)accuracy.getAccuracy()) / (double)accuracy.getAccuracy();
    }

    protected void setValue(int value) {

        this.value = value;
    }
}
