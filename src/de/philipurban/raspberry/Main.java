package de.philipurban.raspberry;

import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import de.philipurban.raspberry.fuserver.CommunicationManager;
import de.philipurban.raspberry.iointerface.*;
import de.philipurban.raspberry.iointerface.functions.measurement.types.Measurement;
import de.philipurban.raspberry.iointerface.functions.output.types.Output;
import de.philipurban.raspberry.measurements.MeasurementEffect;
import de.philipurban.raspberry.measurements.MeasurementManager;
import de.philipurban.raspberry.output.OutputEffect;
import de.philipurban.raspberry.output.OutputManager;

import java.io.IOException;

public class Main {

    private static MeasurementManager measurementManager;
    private static OutputManager outputManager;

    public static void main(String[] args) throws InterruptedException, IOException {

        ConsoleOutput.printLine("Started I2C interface communication...", true);

        final GpioController gpio = GpioFactory.getInstance();
        final I2CBus bus = I2CFactory.getInstance(I2CBus.BUS_1);

        // GPIO Pins
        final GpioPinDigitalOutput statusLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "Status LED", PinState.LOW);
        final GpioPinDigitalInput shutdownButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, "Shutdown Button", PinPullResistance.PULL_DOWN);

        // Status LED
        statusLED.high();
        shutdownButton.addListener(new ShutdownButton(statusLED));
        Runtime.getRuntime().addShutdownHook(new ShutdownHook(statusLED));

        // Load config
        if(args.length > 0) {
            Configuration.load(args[0]);
        }
//        Configuration.get().set(Configuration.Parameter.SERVER_URL, "http://beta.philipurban.de/ecobalance");
//        Configuration.get().set(Configuration.Parameter.SERVER_URL, "http://imi-dev.imi.kit.edu:9973");
        Configuration.get().set(Configuration.Parameter.LAST_COMMAND_NUMBER, 0);
        Configuration.get().set(Configuration.Parameter.INTERNAL_MEASUREMENT_INTERVAL_MS, 500); // internal measurement interval -- DO NOT CHANGE
//        Configuration.get().set(Configuration.Parameter.MEASUREMENT_INTERVAL_MS, 20000); // init measurement interval to 20 seconds

        // Initialize IO-Interface
        final IOInterfaceManager ioInterfaceManager = new IOInterfaceManager(bus);
        ioInterfaceManager.registerIOInterface(0x02);
        ioInterfaceManager.startInterfaceCommunication();
        IOInterface ioInterface = ioInterfaceManager.getIOInterface(0x02);
        Measurement frequencyMeasurement1 = ioInterface.getMeasurementManager().getMeasurementByName(IOCatalog.InterfaceInput.FREQUENCY_1.getName());
        Measurement voltageMeasurement1 = ioInterface.getMeasurementManager().getMeasurementByName(IOCatalog.InterfaceInput.VOLTAGE_1.getName());
        Measurement voltageMeasurement2 = ioInterface.getMeasurementManager().getMeasurementByName(IOCatalog.InterfaceInput.VOLTAGE_2.getName());
        Measurement currentMeasurement1 = ioInterface.getMeasurementManager().getMeasurementByName(IOCatalog.InterfaceInput.CURRENT_1.getName());
        Measurement currentMeasurement2 = ioInterface.getMeasurementManager().getMeasurementByName(IOCatalog.InterfaceInput.CURRENT_2.getName());

        Output voltageOutput1 = ioInterface.getOutputManager().getOutputByName(IOCatalog.InterfaceOutput.VOLTAGE_1.getName());
        Output currentOutput = ioInterface.getOutputManager().getOutputByName(IOCatalog.InterfaceOutput.CURRENT_1.getName());

        // Initialize FU-Server communication
        CommunicationManager communicationManager = new CommunicationManager();
        communicationManager.startCommunication();

        measurementManager = new MeasurementManager(communicationManager);
        outputManager = new OutputManager(communicationManager);

        // init selected rpm output channel
        String rpmOutputChannel = Configuration.get().getString(Configuration.Parameter.RPM_OUTPUT_CHANNEL);
        if(rpmOutputChannel.equalsIgnoreCase(currentOutput.getName())) {
            outputManager.addOutput(currentOutput, OutputEffect.RPM, Configuration.get().getDouble(Configuration.Parameter.RPM_OUTPUT_MULTIPLIER));
        }
        else if(rpmOutputChannel.equalsIgnoreCase(voltageOutput1.getName())) {
            outputManager.addOutput(voltageOutput1, OutputEffect.RPM, Configuration.get().getDouble(Configuration.Parameter.RPM_OUTPUT_MULTIPLIER));
        }

        // init selected energy measurement channel
        String energyChannel = Configuration.get().getString(Configuration.Parameter.ENERGY_INPUT_CHANNEL);
        if(energyChannel.equalsIgnoreCase(voltageMeasurement1.getName())) {
            measurementManager.addMeasurement(voltageMeasurement1, MeasurementEffect.ENERGY);
        }
        else if(energyChannel.equalsIgnoreCase(voltageMeasurement2.getName())) {
            measurementManager.addMeasurement(voltageMeasurement2, MeasurementEffect.ENERGY);
        }
        else if(energyChannel.equalsIgnoreCase(currentMeasurement1.getName())) {
            measurementManager.addMeasurement(currentMeasurement1, MeasurementEffect.ENERGY);
        }
        else if(energyChannel.equalsIgnoreCase(frequencyMeasurement1.getName())) {
            measurementManager.addMeasurement(frequencyMeasurement1, MeasurementEffect.ENERGY);
        }

        // init selected energy measurement channel
        String rpmChannel = Configuration.get().getString(Configuration.Parameter.RPM_INPUT_CHANNEL);
        if(rpmChannel.equalsIgnoreCase(voltageMeasurement1.getName())) {
            measurementManager.addMeasurement(voltageMeasurement1, MeasurementEffect.RPM);
        }
        else if(energyChannel.equalsIgnoreCase(voltageMeasurement2.getName())) {
            measurementManager.addMeasurement(voltageMeasurement2, MeasurementEffect.RPM);
        }
        else if(rpmChannel.equalsIgnoreCase(currentMeasurement1.getName())) {
            measurementManager.addMeasurement(currentMeasurement1, MeasurementEffect.RPM);
        }
        else if(rpmChannel.equalsIgnoreCase(frequencyMeasurement1.getName())) {
            measurementManager.addMeasurement(frequencyMeasurement1, MeasurementEffect.RPM);
        }

        for(;;) {

            measurementManager.triggerMeasurement();
        }
    }

    public static MeasurementManager getMeasurementManager() {

        return measurementManager;
    }

    public static OutputManager getOutputManager() {

        return outputManager;
    }
}
