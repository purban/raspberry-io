package de.philipurban.raspberry.iointerface;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

/**
 * @author Philip Urban
 */
public class ShutdownHook extends Thread {

    private GpioPinDigitalOutput statusLED;

    public ShutdownHook(GpioPinDigitalOutput statusLED) {

        this.statusLED = statusLED;
    }

    @Override
    public void run() {

        statusLED.low();
    }
}
