package de.philipurban.raspberry.measurements;

/**
 * @author Philip Urban
 */
public enum MeasurementEffect {

    RPM,
    ENERGY
}
