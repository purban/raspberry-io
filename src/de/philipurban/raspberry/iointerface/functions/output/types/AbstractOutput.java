package de.philipurban.raspberry.iointerface.functions.output.types;

import de.philipurban.raspberry.iointerface.IOInterface;
import de.philipurban.raspberry.iointerface.functions.AbstractHardwareFunction;

/**
 * @author Philip Urban
 */
public abstract class AbstractOutput extends AbstractHardwareFunction implements Output {

    private int value;
    private byte[] rawValue = new byte[IOInterface.BYTE_SIZE];

    protected AbstractOutput(String name, int startAddress) {

        super(name, startAddress);
    }

    @Override
    public void setValue(int value) {

        this.value = value;
        calculateRawValue();
    }

    @Override
    public int getValue() {

        return value;
    }

    protected void setRawValue(byte[] rawValue) {

        this.rawValue = rawValue;
    }

    @Override
    public byte[] getRawValue() {

        return rawValue;
    }
}
