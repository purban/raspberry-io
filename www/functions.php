<?php

function getVersion() {

	$content = file_get_contents("./config/version.info");
	return $content;
}

function isOnline($domain) {

    $domain .= "/servertime";

	//check, if a valid url is provided
	if(!filter_var($domain, FILTER_VALIDATE_URL))
	{
		   return false;
	}

	//initialize curl
	$curlInit = curl_init($domain);
	curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
	curl_setopt($curlInit,CURLOPT_HEADER,true);
	curl_setopt($curlInit,CURLOPT_NOBODY,true);
	curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
	
	//get answer
	$response = curl_exec($curlInit);

	$httpCode = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
	curl_close($curlInit);

    if($httpCode == 200) {
        return true;
    }

	return false;
}

function isRunning($process) {

    exec("ps aux | grep ".$process, $result);

    if(count($result) > 2) {
        return true;
    }
    return false;
}

function restartService() {
    exec("/var/ecobalance/raspberry-io/run.sh > /dev/null &");
    sleep(1);
}

function stopService() {
    exec("/var/ecobalance/raspberry-io/stop.sh");
    sleep(1);
}

function preVariable($var) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

?>
