package de.philipurban.raspberry.fuserver.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Philip Urban
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandInformation {

    public String name();
}
