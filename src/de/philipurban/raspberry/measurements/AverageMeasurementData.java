package de.philipurban.raspberry.measurements;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Philip Urban
 */
public class AverageMeasurementData {

    private long startTime;
    private long endTime;
    private double energy;
    private double rpm;

    public AverageMeasurementData() {

    }

    public AverageMeasurementData(long startTime, long endTime, double energy, double rpm) {

        this.startTime = startTime;
        this.endTime = endTime;
        this.energy = energy;
        this.rpm = rpm;
    }

    public void setStartTime(long startTime) {

        this.startTime = startTime;
    }

    public void setEndTime(long endTime) {

        this.endTime = endTime;
    }

    public void setEnergy(double energy) {

        this.energy = energy;
    }

    public void setRpm(double rpm) {

        this.rpm = rpm;
    }

    public long getStartTime() {

        return startTime;
    }

    public long getEndTime() {

        return endTime;
    }

    public double getEnergy() {

        return energy;
    }

    public double getRpm() {

        return rpm;
    }

    private String getFormattedDate(long timestamp) {

        SimpleDateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formattedDate.format(new Date(timestamp));
    }

    public String getFormattedStartTime() {

        return getFormattedDate(startTime);
    }

    public String getFormattedEndTime() {

        return getFormattedDate(endTime);
    }
}
