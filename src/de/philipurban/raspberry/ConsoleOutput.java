package de.philipurban.raspberry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
public class ConsoleOutput {

    private static final long ERROR_DELAY = 1000;
    private static Map<String, Long> lastTransmit = new HashMap<>();

    private static String getTimeString() {

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static void print(String message, boolean printTime) {

        if(printTime) {
            System.out.print("[" + getTimeString() + "] " + message);
        }
        else {
            System.out.print(message);
        }
    }

    public static void printLine(String message, boolean printTime) {

        if(printTime) {
            System.out.println("[" + getTimeString() + "] " + message);
        }
        else {
            System.out.println(message);
        }
    }

    public static void printError(String message, String errorName) {

        if(lastTransmit.containsKey(errorName)) {
            if((System.currentTimeMillis() - lastTransmit.get(errorName)) < ERROR_DELAY) {
                return;
            }
        }
        lastTransmit.put(errorName, System.currentTimeMillis());
        printLine(message, true);
    }
}
