package de.philipurban.raspberry.fuserver;

import com.google.gson.Gson;
import de.philipurban.raspberry.ConsoleOutput;
import de.philipurban.raspberry.fuserver.messages.Message;
import de.philipurban.raspberry.fuserver.messages.ParameterType;
import de.philipurban.raspberry.fuserver.messages.ProcessStatus;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @author Philip Urban
 */
public class HttpRequestTask implements Runnable {

    private CommunicationManager manager;
    private int index = 0;

    public HttpRequestTask(CommunicationManager manager) {

        this.manager = manager;
    }

    @Override
    public void run() {

        while(true) {

            // increase index
            increaseIndex();

            // get message to process
            QueuedMessage queuedMessage = manager.getQueuedMessages().get(index);
            // check if message is null
            if(queuedMessage == null) continue;
            Message message = queuedMessage.getMessage();
            // prepare message
            message.prepareMessage();
            // check if message is disabled
            if(message.isDisabled()) continue;
            // check if message has cooldown
            if((int)((System.currentTimeMillis() - queuedMessage.getLastProcessing())/1000L) <= queuedMessage.getInterval()) continue;

            // http request
            try {
                ConsoleOutput.print("Start HttpRequest (" + message.getParameterType().name() + "): " + message.getClass().getSimpleName(), true);

                String parameters = "";
                for(Map.Entry<String, String> entry : message.getParameterList().entrySet()) {
                    if(!parameters.isEmpty()) parameters += "&";
                    String value = entry.getValue();
                    if(value == null) value = "";
                    parameters += entry.getKey() + "=" + URLEncoder.encode(value, "UTF-8");
                }
//                ConsoleOutput.printLine("Parameters: " + parameters, true);

                String getParameter = "";
                if(message.getParameterType() == ParameterType.GET) {
                    getParameter = "?" + parameters;
                }

                URL url = new URL(message.getUrl() + getParameter);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setConnectTimeout(10000);
                connection.setReadTimeout(10000);
                connection.setRequestMethod(message.getParameterType().name());
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                if(message.getParameterType() == ParameterType.POST) {
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
                    connection.setRequestProperty("Content-Length", String.valueOf(parameters.length()));
                }

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(parameters);
                writer.flush();

                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()) );

                String response = "";
                for ( String line; (line = reader.readLine()) != null; )
                {
                    response += line;
                }

                writer.close();
                reader.close();

                Gson gson = new Gson();
//                ConsoleOutput.printLine("Server Response: " + response, true);
                Object responseObj = gson.fromJson(response, message.getResponseClass());
                if(responseObj == null) throw new Throwable("Wrong Server Response - Unexpected JSON Content");
                ConsoleOutput.print(" -> Successful: ", false);
                ProcessStatus processStatus = message.processResponse(responseObj);
                queuedMessage.setSuccessful(processStatus.isSuccessful());
                ConsoleOutput.printLine(processStatus.getMessage(), false);
            } catch (Throwable e) {

                queuedMessage.setSuccessful(false);
                ConsoleOutput.printLine(" -> Failed: " + e.getMessage(), false);
            }

            // set processed
            queuedMessage.setProcessed();
        }
    }

    private void increaseIndex() {

        index++;
        if(index == manager.getQueuedMessages().size()) index = 0;
    }
}
