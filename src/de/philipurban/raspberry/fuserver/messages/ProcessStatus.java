package de.philipurban.raspberry.fuserver.messages;

/**
 * @author Philip Urban
 */
public class ProcessStatus {

    private boolean successful;
    private String message;

    public ProcessStatus() {
    }

    public ProcessStatus(boolean successful, String message) {

        this.successful = successful;
        this.message = message;
    }

    public void setSuccessful(boolean successful) {

        this.successful = successful;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public void set(String message, boolean successful) {

        this.message = message;
        this.successful = successful;
    }

    public boolean isSuccessful() {

        return successful;
    }

    public String getMessage() {

        return message;
    }
}
