package de.philipurban.raspberry.iointerface.functions;

/**
 * @author Philip Urban
 */
public interface HardwareFunction {

    public String getName();

    public int getStartAddress();

    public int getByteSize();
}
