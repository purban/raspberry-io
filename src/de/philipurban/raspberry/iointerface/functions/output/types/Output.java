package de.philipurban.raspberry.iointerface.functions.output.types;

import de.philipurban.raspberry.iointerface.functions.HardwareFunction;

/**
 * @author Philip Urban
 */
public interface Output extends HardwareFunction {

    public void setValue(int value);

    public int getValue();

    public void calculateRawValue();

    public byte[] getRawValue();

    public boolean equals(Object o);

    public int hashCode();
}
