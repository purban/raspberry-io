package de.philipurban.raspberry.iointerface;

/**
 * @author Philip Urban
 */
public final class IOCatalog {

    public static enum InterfaceInput {

        FREQUENCY_1("FREQUENCY_1", 0 * IOInterface.BYTE_SIZE),
        VOLTAGE_1("VOLTAGE_1", 1 * IOInterface.BYTE_SIZE),
        VOLTAGE_2("VOLTAGE_2", 2 * IOInterface.BYTE_SIZE),
        CURRENT_1("CURRENT_1", 3 * IOInterface.BYTE_SIZE),
        CURRENT_2("CURRENT_2", 4 * IOInterface.BYTE_SIZE);
        ;

        private String name;
        private int address;

        private InterfaceInput(String name, int address) {

            this.name = name;
            this.address = address;
        }

        public String getName() {

            return name;
        }

        public int getAddress() {

            return address;
        }
    }

    public static enum InterfaceOutput {

        VOLTAGE_1("VOLTAGE_1", 0 * IOInterface.BYTE_SIZE),
        CURRENT_1("CURRENT_1", 1 * IOInterface.BYTE_SIZE)
        ;

        private String name;
        private int address;

        private InterfaceOutput(String name, int address) {

            this.name = name;
            this.address = address;
        }

        public String getName() {

            return name;
        }

        public int getAddress() {

            return address;
        }
    }
}
