package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

/**
 * @author Philip Urban
 */
@CommandInformation(
        name = "warning"
)
public class WarningCommand extends SimpleCommand {

    @Override
    public ProcessStatus process(GetCommandResponse response) {

        return new ProcessStatus(true, "Received warning: " + response.getParameter1());
    }
}
