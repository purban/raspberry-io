package de.philipurban.raspberry.fuserver.messages.response;

/**
 * @author Philip Urban
 */
public class RegisterVfdResponse {

    private int vfdid;
    private String description;

    public String getVfdid() {

        return String.valueOf(vfdid);
    }

    public String getDescription() {

        return description;
    }
}
