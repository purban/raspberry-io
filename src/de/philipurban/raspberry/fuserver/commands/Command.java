package de.philipurban.raspberry.fuserver.commands;

import de.philipurban.raspberry.fuserver.messages.ProcessStatus;
import de.philipurban.raspberry.fuserver.messages.response.GetCommandResponse;

/**
 * @author Philip Urban
 */
public interface Command {

    public String getName();

    public ProcessStatus process(GetCommandResponse response);
}
