package de.philipurban.raspberry.iointerface;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import de.philipurban.raspberry.ConsoleOutput;
import de.philipurban.raspberry.iointerface.functions.measurement.MeasurementManager;
import de.philipurban.raspberry.iointerface.functions.measurement.types.SimpleMeasurement;
import de.philipurban.raspberry.iointerface.functions.output.OutputManager;
import de.philipurban.raspberry.iointerface.functions.output.types.SimpleOutput;

import java.io.IOException;

/**
 * @author Philip Urban
 *
 * //TODO !!!IMPORTANT -- READ THIS!!!
 * I2C bus speed must be reduced to max. 50000KHz
 * Following command can be used: sudo modprobe -r i2c_bcm2708 && sudo modprobe i2c_bcm2708 baudrate=50000
 */
public class IOInterface {

    public final static int BYTE_SIZE = 12;

    private I2CDevice device;
    private MeasurementManager measurementManager;
    private OutputManager outputManager;

    public IOInterface(I2CBus bus, int deviceAddress) {

        try {
            this.device = bus.getDevice(deviceAddress);
        } catch (IOException e) {
            ConsoleOutput.printError("Error while initializing i2c interface device: " + e.getMessage(), "initializing");
        }

        measurementManager = new MeasurementManager();
        outputManager = new OutputManager();

        // register all hardware implemented measurements
        measurementManager.registerMeasurement(new SimpleMeasurement(IOCatalog.InterfaceInput.FREQUENCY_1));
        measurementManager.registerMeasurement(new SimpleMeasurement(IOCatalog.InterfaceInput.VOLTAGE_1));
        measurementManager.registerMeasurement(new SimpleMeasurement(IOCatalog.InterfaceInput.VOLTAGE_2));
        measurementManager.registerMeasurement(new SimpleMeasurement(IOCatalog.InterfaceInput.CURRENT_1));
        measurementManager.registerMeasurement(new SimpleMeasurement(IOCatalog.InterfaceInput.CURRENT_2));

        // register all hardware implemented outputs
        outputManager.registerOutput(new SimpleOutput(IOCatalog.InterfaceOutput.VOLTAGE_1));
        outputManager.registerOutput(new SimpleOutput(IOCatalog.InterfaceOutput.CURRENT_1));
    }

    public MeasurementManager getMeasurementManager() {

        return measurementManager;
    }

    public OutputManager getOutputManager() {

        return outputManager;
    }

    public I2CDevice getDevice() {

        return device;
    }
}
