#!/bin/bash

echo "Updating project..."
git stash
git pull
echo "Clean compiling workspace..."
mvn clean
echo "Compiling..."
mvn install
echo "Write version number..."
echo "Build: " + date > www/config/version.info
echo "Fix CHMOD..."
chmod 755 run.sh
chmod 755 stop.sh
chmod 777 www/config/configuration.cfg