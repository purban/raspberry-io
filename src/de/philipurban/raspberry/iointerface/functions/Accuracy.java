package de.philipurban.raspberry.iointerface.functions;

/**
 * @author Philip Urban
 */
public enum Accuracy {

    ONE(1),
    TENTH(10),
    HUNDREDTH(100),
    THOUSANDTH(1000);

    private int accuracy;

    private Accuracy(int accuracy) {

        this.accuracy = accuracy;
    }

    public int getAccuracy() {

        return accuracy;
    }
}
