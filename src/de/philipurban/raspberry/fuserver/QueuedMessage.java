package de.philipurban.raspberry.fuserver;

import de.philipurban.raspberry.fuserver.messages.Message;

/**
 * @author Philip Urban
 */
public class QueuedMessage {

    private Message message;
    private int interval;
    private long lastProcessing = 0;
    private boolean successful = false;

    public QueuedMessage(Message message, int interval) {

        this.message = message;
        this.interval = interval;
    }

    public Message getMessage() {

        return message;
    }

    public int getInterval() {

        return interval;
    }

    public long getLastProcessing() {

        return lastProcessing;
    }

    public boolean wasSuccessful() {

        return successful;
    }

    public void setProcessed() {

        lastProcessing = System.currentTimeMillis();
    }

    public void setSuccessful(boolean successful) {

        this.successful = successful;
    }
}
