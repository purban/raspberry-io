package de.philipurban.raspberry.output;

import de.philipurban.raspberry.fuserver.CommunicationManager;
import de.philipurban.raspberry.iointerface.functions.output.types.Output;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Philip Urban
 */
public class OutputManager {

    private CommunicationManager communicationManager;
    private Map<OutputEffect, OutputObject> outputObjects = new HashMap<>();

    public OutputManager(CommunicationManager communicationManager) {

        this.communicationManager = communicationManager;
    }

    public void addOutput(Output output, OutputEffect outputEffect, double multiplier) {

        outputObjects.put(outputEffect, new OutputObject(output, outputEffect, multiplier));
    }

    public OutputObject getOutputObject(OutputEffect effect) {

        OutputObject outputObject = outputObjects.get(effect);
        return outputObject;
    }
}
