package de.philipurban.raspberry.output;

import de.philipurban.raspberry.iointerface.functions.output.types.Output;

/**
 * @author Philip Urban
 */
public class OutputObject {

    private Output output;
    private OutputEffect effect;
    private double multiplier;

    public OutputObject(Output output, OutputEffect effect, double multiplier) {

        this.output = output;
        this.effect = effect;
        this.multiplier = multiplier;
    }

    public Output getOutput() {

        return output;
    }

    public OutputEffect getEffect() {

        return effect;
    }

    public void setValue(int value) {

        output.setValue((int)(value * multiplier));
    }
}
