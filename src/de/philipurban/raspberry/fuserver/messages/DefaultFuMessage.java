package de.philipurban.raspberry.fuserver.messages;

import de.philipurban.raspberry.Configuration;

/**
 * @author Philip Urban
 */
public abstract class DefaultFuMessage implements Message {

    private boolean disabled = false;
    private String subUrl;
    private ParameterType parameterType;
    private Class responseClass;

    protected DefaultFuMessage() {

        FuMessageInformation annotation = getClass().getAnnotation(FuMessageInformation.class);

        subUrl = annotation.subUrl();
        parameterType = annotation.parameterType();
        responseClass = annotation.responseClass();
    }

    @Override
    public final Class getResponseClass() {

        return responseClass;
    }

    @Override
    public final ParameterType getParameterType() {

        return parameterType;
    }

    @Override
    public final String getUrl() {

        return Configuration.get().getString(Configuration.Parameter.SERVER_URL) + "/" + subUrl;
    }

    @Override
    public boolean isDisabled() {

        return disabled;
    }

    public void setDisabled(boolean disabled) {

        this.disabled = disabled;
    }
}
